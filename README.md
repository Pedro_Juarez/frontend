# INTRODUCCIÓN #

Este es el proyecto para el test técnico de Proyecta tu futuro.
El Objetivo de este test es comprobar la resiliencia y la capacidad de manejar un entorno de trabajo parecido a al que se usa en la empresa. Se pondrá a prueba el uso de las líneas de comando para ejecutar, compilar y modificar un proyecto ya creado, además del uso simple de herramientas tipo Git, y la capacidad de resolver problemas por sí mismos usando un buscador como Google.  
  
Este proyecto está hecho con [Angular](https://angular.io/) y [Angular CLI](https://angular.io/cli) y manejado por [NPM](https://www.npmjs.com/)

### INSTRUCCIONES ###

Para llevar a cabo este proyecto, primero crea un Fork de este mismo para que lo puedas modificar a tu manera.  
  
Después, analízalo y modifícalo para que pueda escuchar peticiones del servidor que también se necesitará hacer [desde aquí](https://bitbucket.org/Nahtanoj84/backend/src/master/). El punto es hacer que escuche en JSON una respuesta del menú para que este la construya como debería.  
  
Aconsejamos que uses el puerto 7431.
  
Modifica la construcción del menú para que reciba un ícono de [Font Awesome](https://fontawesome.com/icons?d=gallery&p=2) relacionado en vez de una imágen inexistente para cada sección de este. Solo se necesitan por lo menos 5 íconos en el menú. Puedes elegir si el menú recibirá un ícono o no.  
  
Cuando termines, sube tu proyecto escribiendo y publicando los cambios en tu Fork, y una copia de la compilación (con el comando `ng build --prod`) y envía la URL de tu proyecto, junto con un ZIP del proyecto compilado a [jonathan.leyva@proyectatufuturo.com](mailto:jonathan.leyva@proyectatufuturo.com)

### Notas ###

Tip: Navega por el historial de Commits de este proyecto. Se hicieron con la intensión de señalar partes importantes del código.


#### Ejemplo del JSON esperado por el programa ####

```json
{
    menu: [
        {
            name: 'Usuarios',
            icon: '/img/1.png',
            badge: undefined,
            menu: [
                {
                    name: 'Crear usuarios',
                    icon: '/img/2.png',
                    badge: 0,
                    menu: undefined
                },
                {
                    name: 'Modificar usuarios',
                    icon: '/img/3.png',
                    badge: 0,
                    menu: undefined
                },
                {
                    name: 'Usuarios borrados',
                    icon: '/img/4.png',
                    badge: 0,
                    menu: undefined
                }
            ]
        },
        {
            name: 'Chat',
            icon: '/img/1.png',
            badge: 3,
            menu: [
                {
                    name: 'Mensajes',
                    icon: '/img/2.png',
                    badge: 3,
                    menu: undefined
                },
                {
                    name: 'Amigos en línea',
                    icon: '/img/2.png',
                    badge: 9,
                    menu: undefined
                },
                {
                    name: 'Comunicados',
                    icon: '/img/2.png',
                    badge: 0,
                    menu: undefined
                }
            ]
        },
        {
            name: 'Mi cuenta',
            icon: '/img/1.png',
            badge: 3,
            menu: [
                {
                    name: 'Configuración',
                    icon: '/img/2.png',
                    badge: 0,
                    menu: [
                    {
                        name : 'Contraseña',
                        icon: '',
                        badge: undefined,
                        menu: undefined
                    },
                    {
                        name : 'Correo',
                        icon: '',
                        badge: undefined,
                        menu: undefined
                    },
                    {
                        name : 'Eliminar cuenta',
                        icon: '',
                        badge: undefined,
                        menu: undefined
                    },
                    ]
                },
                {
                    name: 'Mis datos personales',
                    icon: '/img/2.png',
                    badge: 0,
                    menu: undefined
                },
                {
                    name: 'Cerrar sesión',
                    icon: '/img/2.png',
                    badge: 0,
                    menu: undefined
                }
            ]
        }
    ]
}
```

##### Requerimientos extra #####
Como requerimiento extra (opcional), actualiza el proyecto y sus dependencias para que cuando se haga el comando `npm audit` este regrese 0 vulnerabilidades detectadas.
  
Si lo deseas, escribe aquí más abajo una sección de comentarios de los cambios que hiciste y qué dificultades tuviste (si es que aplica).

##### Comentarios #####